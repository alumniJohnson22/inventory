package com.m3.training.inventory;

public class Driver {
	
	public static void main(String[] args) throws InventoryException{
		Item item = Item.createItem("1", "lawn darts", 100);
		ItemArchive archive = new ItemArchive();
		archive.addCurrent(item);
		System.out.println("Purchase item " + archive.allocate(item, 3));
		archive.addRecall(item);
		archive.allocate(item, 3);
	}

}
