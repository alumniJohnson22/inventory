package com.m3.training.inventory;

public class Item {
	
	private String sku;
	private String name;
	private int count;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	private Item() {
	}

	public static Item createItem(String sku, String name, int count) {
		Item item = new Item();
		item.setCount(count);
		item.setName(name);
		item.setSku(sku);
		return item;
	}
	
	public String toString() {
		return "Name " + name + " sku "  + sku + " Count " + count;
	}
	
	
}
