package com.m3.training.inventory;

public class ItemArchive {
	private Inventory currentItems;
	private Inventory recalledItems;
	
	public ItemArchive() {
		currentItems = new Inventory();
		recalledItems = new Inventory();
	}

	public Item addRecall(Item item) {
		if (currentItems.containsKey(item.getSku())) {
			currentItems.delete(item);
		}
		return recalledItems.add(item);
	}
	
	public Item addCurrent(Item item) {
		Item addedItem = currentItems.add(item);
		return addedItem;
	}
	
	public boolean containsKeyCurrent(Object arg0) {
		return currentItems.containsKey(arg0);
	}

	public boolean containsValueCurrent(Object arg0) {
		return currentItems.containsValue(arg0);
	}

	public boolean equalsCurrent(Object arg0) {
		return currentItems.equals(arg0);
	}

	public Item getCurrent(Item item) {
		return currentItems.get(item);
	}

	public Item allocate(Item item, int requestCount) throws InventoryException {
		Item inventoryItem;
		if (recalledItems.containsKey(item.getSku())) {		
			String msg = "Item " + item + " is no longer available due to recall.";
			throw new InventoryException(msg);
		}
		if (!currentItems.containsKey(item.getSku())) {
			// add item
		}
		inventoryItem = currentItems.get(item);
		// process allocation 
		return inventoryItem;
	}

	public boolean isEmptyCurrent() {
		return currentItems.isEmpty();
	}

	public boolean containsKeyRecall(Object arg0) {
		return recalledItems.containsKey(arg0);
	}

	public boolean containsValueRecall(Object arg0) {
		return recalledItems.containsValue(arg0);
	}

	public boolean equalsRecall(Object arg0) {
		return recalledItems.equals(arg0);
	}

	public Item getRecall(Item item) {
		return recalledItems.get(item);
	}



	public boolean isEmptyRecall() {
		return recalledItems.isEmpty();
	}


	
	
	
}
