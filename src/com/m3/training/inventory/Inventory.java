package com.m3.training.inventory;

import java.util.HashMap;

public class Inventory {

	private HashMap<String, Item> map;
	public Inventory() {
		map = new HashMap<>();
	}
	public boolean containsKey(Object arg0) {
		return map.containsKey(arg0);
	}
	public boolean containsValue(Object arg0) {
		return map.containsValue(arg0);
	}
	public Item get(Item item) {
		return map.get(item.getSku());
	}
	public boolean isEmpty() {
		return map.isEmpty();
	}
	public Item add(Item item) {
		String key = item.getSku();
		map.put(key, item);
		return item;
	}
	public Item delete(Item item) {
		return map.remove(item.getSku());
	}

}
